
extern Adafruit_BMP085 bmp;    // Pressure & temp sensor                     

// Print vars to serial port - For debugging
void Debug_Print() 
{
    //Serial.print(F("C = "); 
    //Serial.println(max6675.readCelsius());
    //Serial.print(F("RPM = "); 
    //Serial.print(g_rpm);

    Serial.print(F("Temperature = "));
    Serial.print(bmp.readTemperature());
    Serial.println(F(" degC"));
    
    Serial.print(F("Pressure = "));
    Serial.print(bmp.readPressure());
    Serial.println(F(" Pa"));
    
    // Calculate altitude assuming 'standard' barometric
    // pressure of 1013.25 millibar = 101325 Pascal
    Serial.print(F("Altitude = "));
    Serial.print(bmp.readAltitude());
    Serial.println(F(" meters"));

    Serial.print(F("Pressure at sea level (calculated) = "));
    Serial.print(bmp.readSealevelPressure());
    Serial.println(F(" Pa"));

  // you can get a more precise measurement of altitude
  // if you know the current sea level pressure which will
  // vary with weather and such. If it is 1015 millibars
  // that is equal to 101500 Pascals.
    Serial.print(F("Real altitude = "));
    Serial.print(bmp.readAltitude(101500));
    Serial.println(F(" meters"));
    
    Serial.println();
    //delay(500);
}
