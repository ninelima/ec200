// include the library
#include <Adafruit_BMP085.h>
#include <Wire.h>
#include <SPI.h>
#include <Servo.h>
#include "max6675.h" // max6675.h file is part of the library that you should download from Robojax.com

#include <Adafruit_GFX.h>    // Core graphics library
#include <Adafruit_ST7735.h> // Hardware-specific library
//#include <U8g2lib.h>  // 128x128 too big for UNO
#include "debug.h"


// BoM --------------------------
/*
1.
128x128 LCD Display Module
Altronics Code: Z6524A
https://www.altronics.com.au/p/z6524a-128x128-lcd-display-module/

2.
Barometric Air Pressure Sensor - BMP180
Altronics Code: Z6395
https://www.altronics.com.au/p/z6395-barometric-air-pressure-sensor-breakout-for-arduino/

3.
CJMCU MPXV7002DP Airspeed Sensor
https://electropeak.com/airspeed-sensor-mpxv7002-apm

4.
MPX5010 Differential Pressure Sensor

5.
Rotary Encoder for Arduino, 
ESP32, ESP8266
https://www.amazon.com/stores/page/F69417CA-B562-49B7-81D5-F21D8F5FC2C0/search?ref_=ast_bln&terms=rotary%20encoder

6.
Adruino Pro Micro

7.
270 degree Servo  // TS90 MD   

8.
180 degree Servo  // ESky



// Circuit NETLIST --------------
/*
// Nokia TFT Display
//Adafruit_ST7735(int8_t cs, int8_t dc, int8_t rst);
Adafruit_ST7735 tft = Adafruit_ST7735(TFT_CS,  TFT_DC, TFT_RST);
#define TFT_CS     10
#define TFT_DC     9
#define TFT_RST    8  


// Nokia TFT Display
Adafruit_ST7735.VCC.1	+5V
Adafruit_ST7735.GND.2	GND
Adafruit_ST7735.LED.6	+5V
Adafruit_ST7735.CLK.7	ARDUINO.D13
Adafruit_ST7735.SDI.8	ARDUINO.D11 
Adafruit_ST7735.RS.9	ARDUINO.D9
Adafruit_ST7735.RST.10	ARDUINO.D8 
Adafruit_ST7735.CS.10	ARDUINO.D10

//Rotary Encoder
R_Encoder.CLK.1	ARDUINO.D2
R_Encoder.DT	ARDUINO.D3
R_Encoder.DW	ARDUINO.D4
R_Encoder.+	ARDUINO.+5V
R_Encoder.GND	ARDUINO.GND

// Differential Pressure Sensor2
PSensor1.OUT	ARDUINO.A0
... 

PSensor1.OUT	ARDUINO.A1
...


*/




// Arduino UNO  - COM 15 
// Arduino Pro Micro use Arduinio Leonardo - COM 21

//-----------------------------------------------------------------------------
// Definitions and assignments
//
#define INTERRUPT_PIN 2  // Only 2 or 3 are interrupt pins 
#define ENCODER_PIN   3  // Only 2 or 3 are interrupt pins 
#define SWITCH_PIN   4   // Only 2 or 3 are interrupt pins 

// Thermocouple MAX6675
#define pinCS0  11  // CS = chip select CS pin
#define pinSO0  12  // SO=Serial Out
#define pinSCK0 13  // SCK = Serial Clock pin

// Define SPI devices vars
#define MOSI 11    // LCD use
#define MISO 12    // SO = Serial Out
#define CLK  13    // SCK = Serial Clock pin

// SPI Chip Selects
#define CS_TC1  6   // Termocouple 1
#define CS_TC2  7   // Termocouple 2
#define CS_TC3  8   // Termocouple 3
//#define CS_LCD  9   // LCD Display

#define TIMER 100     //500   // Timer interval in ms

// Servo pins. Must be PWM pins
#define PIN_ASI 6  
#define PIN_VSI 5  

#define BLACK   0x0000
#define RED     0x001F
#define BLUE    0xF800
#define GREEN   0x07E0
#define YELLOW  0x07FF
#define PURPLE  0xF81F
#define CYAN    0xFFE0  
#define WHITE   0xFFFF

// Nokia TFT Display
// create an instance of the library
#define cs   10
#define dc   9
#define rst  8

#define TFT_CS     10
#define TFT_DC     9
#define TFT_RST    8  

#define VNE 130  // Maximum speed on the scale


MAX6675 max6675_3(/*clock*/ 13,  /*CS=*/ CS_TC3, /*data MISO*/ 12); // OilT object of MAX6675  
MAX6675 max6675_2(/*clock*/ 13,  /*CS=*/ CS_TC2, /*data MISO*/ 12); // CHT2 object of MAX6675
MAX6675 max6675_1(/*clock*/ 13,  /*CS=*/ CS_TC1, /*data MISO*/ 12); // CHT1 object of MAX6675 

//-----------------------------------------------------------------------------
// Create the objects
//

// Adafruit Pressure sensor
Adafruit_BMP085 bmp;    // Pressure & temp sensor                     
Servo servoIAS;           // create servo object to control a servo
Servo servoVSI;           // create servo object to control a servo


//Adafruit_ST7735(int8_t cs, int8_t dc, int8_t rst);
Adafruit_ST7735 tft = Adafruit_ST7735(TFT_CS,  TFT_DC, TFT_RST);

// char array to print to the screen
char buf[12];

int i = 0; 
int state = 1;        // State machine "state" variable
int MAX_STATE = 3;    // Max number of states for the state machine 


// define some values used by the panel and buttons
int  lcd_key    = 0;
int  _lcd_key   = 0;   // previous key
int  adc_key_in  = 0;
bool lcd_key_pressed  = false;

// Tacho vars
//volatile unsigned int g_revticks = 0;
volatile unsigned long _millis = 0;
volatile double g_revticks = 0;
unsigned int g_ticks = 0;   

// Global vars
double g_alt = 9999.0;
double g_oat;
int g_aoa;
double g_ias; //= VNE;
double g_tas;
double g_vsi; //= -1000;
double g_qnh = 1013.25;  // in millibars (870 - 1050) - Must be a floating point

// Previous values
double _g_alt;  // for VSI calc
long _alt_1000;
long _alt_100;
double _qnh;
int _aoa;



//int _time = 0;  // previous time


String FormatString(String f, String s)
{
    for (int i = 0; i < s.length(); i++) {
      f[i + f.length() - s.length()] = s[i]; 
    }
    return f;
}


//-----------------------------------------------------------------------------
// Interrupt service routines
//
//volatile unsigned long last_time;  // for debouncing

void isr() 
{
  //if ((millis() - last_time) < 50)  // debounce time is 50ms
  //return;  

  // read pin 3
  //if (digitalRead(ENCODER_PIN) == HIGH) g_revticks = g_revticks + 1;
  //else g_revticks = g_revticks - 1;
  // read pin 3
  if (digitalRead(ENCODER_PIN) == LOW) g_qnh = g_qnh + 1;
  else g_qnh = g_qnh - 1;

  //last_time = millis();
}

void attachInterrupt()
{
  attachInterrupt(digitalPinToInterrupt(INTERRUPT_PIN), isr, RISING);  // pin 3 - RISING, FALLING, CHANGE
}

void detachInterrupt()
{
  detachInterrupt(digitalPinToInterrupt(INTERRUPT_PIN));  // pin 3
}


//--------------------------
// The page layout
// window ~ 128 x 96
//

//--------------------------
// Altitude
//

#define SCREEN_Y 35 //20

void print_Altitude(long alt, int16_t qnh, uint16_t color)
{
  int x, y;
  long alt_1000;
  long alt_100;

  if (alt < 0) { 
    alt = abs(alt);
    color = RED;
  }
   
  alt_1000 = alt / 1000;
  alt_100 = alt % 1000;
  
  alt_100 = alt_100 / 10 * 10;      // to the nearest 10. Ie last digit is always 0
  //if (alt > 100) alt_100 = alt_100 / 10 * 10;      // to the nearest 10. Ie last digit is always 0
  //else alt_100 = alt_100 / 5 * 5;      // to the nearest 10. Ie last digit is always 0 
  // if (alt > 10000) alt_100 = alt_100 / 100 * 100;  // to the nearest 100. Ie last 2 digits are always 0

  tft.setTextColor(color);

  if (_alt_1000 != alt_1000) {
    tft.setTextSize(4);
    x = 0; y = SCREEN_Y; //20;

    sprintf(buf, "%2u", _alt_1000);  
    tft.setTextColor(BLACK);
    tft.setCursor(x, y);
    tft.println(buf);

    sprintf(buf, "%2u", alt_1000);  
    tft.setTextColor(color);
    tft.setCursor(x, y);
    tft.println(buf);

    _alt_1000 = alt_1000;
  }

  if (_alt_100 != alt_100) {
    tft.setTextSize(3);
    x = 50; y = SCREEN_Y + 5; //25;

    sprintf(buf, "%03u", _alt_100); 
    tft.setTextColor(BLACK);
    tft.setCursor(x, y);
    tft.println(buf);

    sprintf(buf, "%03u", alt_100);
    tft.setTextColor(color);
    tft.setCursor(x, y);
    tft.println(buf);

    // The units - Align with the 1000's text
    tft.setTextColor(WHITE);
    tft.setTextSize(1);

    tft.setCursor(110, SCREEN_Y);
    tft.println("ft");

    _alt_100 = alt_100;
  }

  // QNH - Kollsman window
  if (_qnh != qnh) {
    tft.setTextSize(1);
    x = 80; y = SCREEN_Y + 35; //55;

    sprintf(buf, "%4u", _qnh);  
    tft.setTextColor(BLACK);
    tft.setCursor(x, y);
    tft.println(buf);

    sprintf(buf, "%4u", qnh);  
    tft.setTextColor(WHITE);
    tft.setCursor(x, y);
    tft.println(buf);

    tft.setCursor(110, y);
    tft.println("mb");
    _qnh = qnh;
  }
}

//--------------------------
// True Airspeed - TAS

int _tas;
void print_TAS(int16_t tas, uint16_t color)
{
  int x, y;

  if (_tas != tas) {
    tft.setTextSize(2);
    x = 55; y = SCREEN_Y + 55;// 75;

    sprintf(buf, "%4u", _tas);  
    tft.setTextColor(BLACK);
    tft.setCursor(x, y);
    tft.println(buf);

    sprintf(buf, "%4u", tas);  
    tft.setTextColor(color);
    tft.setCursor(x, y);
    tft.println(buf);

    // The units - Align with the 1000's text
    tft.setTextColor(WHITE);
    tft.setTextSize(1);
    tft.setCursor(110, y);
    tft.println("kt");
    _tas = tas;
  }
}

//--------------------------
// Angle of Attack - AOA

#define AOA_STALL 16
#define AOA_APPROACH 5


void print_AoA(int16_t aoa, uint16_t color)
{
  int x, y, r;

  if (_aoa != aoa) {
    tft.setTextSize(2);
    x = 5; y = SCREEN_Y + 55; //75;

    sprintf(buf, "%2u", _aoa);  
    tft.setTextColor(BLACK);
    tft.setCursor(x, y);
    tft.println(buf);

    if (aoa < AOA_APPROACH) tft.setTextColor(GREEN);
    else if (aoa < AOA_STALL - 5) tft.setTextColor(YELLOW);
    else tft.setTextColor(RED);   // Stall warning, assume stall at 16 deg

    sprintf(buf, "%2u", aoa); 
    //tft.setTextColor(WHITE);
    tft.setCursor(x, y);
    tft.println(buf);

    // The units - Align with the 1000's text
    //tft.setTextColor(WHITE);
    tft.setTextSize(1);
    tft.setCursor(30, y);
    //tft.println((char)0xdf);   //alpha
    tft.println((char)0xf7);    // degree -  247
    _aoa = aoa;

    // The arc
    int x1, y1;
    r = 30;
    y = y - 3;

    // clear the previous plots
    for (int a = 0; a <= AOA_STALL; a+=1) {
      //float theta = map(a, 0, 16, 0, 120) * PI / 180;
      float theta = map(a, 0, 16, -20, 120) * PI / 180;
      x1 = r*cos(theta);
      y1 = r*sin(theta);

      tft.setTextColor(BLACK);
      tft.setCursor(x+10+x1, y+10-y1);
      tft.println(".");
    }

    for (int a = 0; a <= aoa; a+=1) {
      //float theta = map(a, 0, 16, 0, 120) * PI / 180;
      float theta = map(a, 0, 16, -20, 120) * PI / 180;
      x1 = r*cos(theta);
      y1 = r*sin(theta);

      if (a < AOA_APPROACH) tft.setTextColor(GREEN);
      else if (a < AOA_STALL - 5) tft.setTextColor(YELLOW);
      else tft.setTextColor(RED);   // Stall warning, assume stall at 16 deg

      tft.setCursor(x+10+x1, y+10-y1);
      tft.println(".");
    }
  }
}

//--------------------------
// The page
//
void tft_page_1() 
{
  int x, y;

  tft.invertDisplay(false);
  tft.setRotation(0);    // 0, 1, 3
 
  print_Altitude(g_alt, g_qnh, CYAN);
  print_TAS(g_tas, CYAN);
  print_AoA(g_aoa, CYAN);
}

/*
Density altitude example:
Let pressure altitude (P_alt) be 8000 ft, temperature 18°C.

Standard temp (T_s) is given by
T_s=15-.0019812*8000=-0.85°C = (273.15-0.85)°K=272.30°K

Actual temperature (T) is
 18°C=(273.15+18)°K=291.15°K

  Density altitude (D_Alt) = 8000 +(272.30/.0019812)*(1-(272.30/291.15)^0.2349690)
                           = 8000 + 2145 = 10145ft 
or approximately:

  Density Altitude=8000 +118.6*(18+0.85)=10236ft

  D_Alt=P_Alt+118.6*(T-T_s)
  where T and T_s may (both) be either Celsius or Kelvin

For low-speed (M<0.3) airplanes the true airspeed can be obtained from CAS and the density altitude, DA.
TAS = CAS*(rho_0/rho)^0.5=CAS/(1-6.8755856*10^-6 * DA)^2.127940 (DA<36,089.24ft) 
*/

double calcTAS(double ias, double alt, double oat)
{
  // Standard temp (T_s) is given by
  double T_s = 15 - 0.0019812 * alt;  // = -0.85°C = (273.15-0.85)°K=272.30°K
  double D_Alt = alt + 118.6 * (oat - T_s);
  //double TAS = ias / (1 - 6.8755856*10^-6 * D_Alt)^2.127940;   // (DA<36,089.24ft) 
  double TAS = ias / (pow((1.0 - (6.8755856 / 1000000.0) * D_Alt), 2.127940));   // (D_Alt < 36,089.24ft) 
  return TAS;
}

//-----------------------------------------------------------------------------
// Diff sensor 
//

//-------------------------------------
// IAS
//
float V_0 = 5.0; // supply voltage to the pressure sensor
float rho = 1.204; // density of air 

// parameters for averaging and offset
int offset = 0;
int offset_size = 10;
int veloc_mean_size = 20;
int zero_span = 2;

float readDiffSensorIAS() 
{
  float adc_avg = 0; float veloc = 0.0;
  
  // Average a few ADC readings for stability
  for (int i = 0; i < veloc_mean_size; i++){
    adc_avg += analogRead(A0) - offset;
  }
  adc_avg /= veloc_mean_size;
  
  // make sure if the ADC reads below 512, then we equate it to a negative velocity
  if (adc_avg > 512-zero_span and adc_avg < 512 + zero_span) {
  } 
  else {
    if (adc_avg <= 512) {
      veloc = -sqrt((-10000.0*((adc_avg/1023.0)-0.5))/rho);
    } 
    else {
      veloc = sqrt((10000.0*((adc_avg/1023.0)-0.5))/rho);
    }
  }
  return 1.94384449 * veloc; // in kts
}

//-------------------------------------
// AoA
//
float readDiffSensorAoA() 
{
  float adc_avg = 0; float aoa = 0.0;
  
  // Average a few ADC readings for stability
  for (int i = 0; i < veloc_mean_size; i++){
    adc_avg += analogRead(A0) - offset;   /// TODO  A1 for AoA
  }
  adc_avg /= veloc_mean_size;
  
  // make sure if the ADC reads below 512, then we equate it to a negative velocity
  if (adc_avg > 512-zero_span and adc_avg < 512 + zero_span) {
    aoa = AOA_STALL;
  } 
  else {
    if (adc_avg <= 512) {
      aoa = AOA_STALL - (-sqrt((-10000.0*((adc_avg/1023.0)-0.5))/rho));
    } 
    else {
      aoa = AOA_STALL - sqrt((10000.0*((adc_avg/1023.0)-0.5))/rho);
    }
  }
  //return 1.94384449 * veloc; // in kts

  return aoa;  // in degrees ... todo
}

//#define AOA_STALL 16
//#define AOA_APPROACH 5

void runDemo()
{
    int iskip;

    // Simulate sweep data for AoA
    iskip = 1; //13; 
    if ((g_ticks / iskip) % 34 < 17) g_aoa = (g_ticks / iskip) % 17;
    else g_aoa = 17 - (g_ticks / iskip) % 17;

    // Simulate sweep data for IAS 
    iskip = 1; 
    if ((g_ticks / iskip) % (2*VNE) < VNE) g_ias = (g_ticks / iskip) % (2*VNE);
    else g_ias = VNE - ((g_ticks / iskip) % 130);

    // Simulate sweep data for VSI from +1000 to - 1000 fpm
    if (g_ticks % 40 < 20) g_vsi = g_ticks % 40;
    else g_vsi = 20 - (g_ticks % 20);
    g_vsi -= 10;  // zero centred 
    g_vsi *= 100; 
}


//-----------------------------------------------------------------------------
// Initialization
//
void setup()
{
  Serial.begin(9600);

  // LCD
  // Use this initializer if you're using TFT
  tft.initR(INITR_BLACKTAB);   // initialize a ST7735S chip, black tab
  tft.fillScreen(ST7735_BLACK);

  //print_labels();
  //draw_buttons();
  //delay(1000);
  // End LCD

  bmp.begin(BMP085_STANDARD);
  if (!bmp.begin()) {
  	while (1) {
    	Serial.println(F("Could not find a valid BMP085 sensor, check wiring!"));
    }
  }

  // attach the servo on pin 5 to the servo object
  servoIAS.attach(PIN_ASI);   //PWM pin 5
  servoVSI.attach(PIN_VSI);   //PWM pin 6
  attachInterrupt();

  pinMode(ENCODER_PIN, INPUT);  // ping 4 used for encoder, 3 for interrupt
  pinMode(SWITCH_PIN, INPUT);

  // Diff Pressure Sensor
  for (int ii = 0; ii < offset_size; ii++) {
    offset += analogRead(A0) - (1023/2);
  }
  offset /= offset_size;

  // Excercise
  servoIAS.write(180);             
  servoVSI.write(0);                
  delay(1000);
  servoIAS.write(0);             
  servoVSI.write(180);                
  delay(1000);
  servoIAS.write(180);             
  servoVSI.write(90);                
  delay(1000);
}

//-----------------------------------------------------------------------------
// Main execution loop
//
//#define BUF 10
//float filter_buf[BUF];

void loop()
{
  // For debugging
	//i = 1000 + 10*(millis() / 1000) % 10000; 

  unsigned long elaps_time = millis() - _millis;
  if (elaps_time >= TIMER) {
    noInterrupts();     // start vital section
    // Calc the revs
    //noInterrupts();
		//g_rpm = (g_revticks * 60000.0 / elaps_time);   // calculates rpm
    interrupts();  // end the vital section

    // Format for display
    g_oat = (int) bmp.readTemperature();    // in deg C
    //g_oat = (int) max6675_1.readCelsius();  // in deg C  - Maybe use this as OAT sensor instead?


    //g_qnh += 0.01;  // debug vsi

    // Calc a running average for Alt
    float alpha = 0.5;
    float acc = ((1 - alpha) * g_alt) + alpha * (bmp.readAltitude(g_qnh * 100) * 3.28084);  
    g_alt = acc;  // in feet

    // Calc a running average for VSI
    if (TIMER * g_revticks > 1000) {
      g_vsi = (double) (60.0 * 1000.0 * (g_alt - _g_alt) / (TIMER * g_revticks));
      Serial.print(g_vsi); Serial.print(" "); Serial.print(g_alt); Serial.print(" "); Serial.print(_g_alt); Serial.print(" "); // Serial.print(a); 
      Serial.println();
      _g_alt = g_alt;
      g_revticks = -1;
    }

    // Calc a running average for AoA
    alpha = 0.1;
    acc = ((1 - alpha) * g_aoa) + alpha * readDiffSensorAoA(); 
    g_aoa = acc;

    // Calc a running average for IAS
    alpha = 0.1;
    acc = ((1 - alpha) * g_ias) + alpha * readDiffSensorIAS(); 
    g_ias = acc;
    if (g_ias < 0) g_ias = 0;

    // Now we can also caclulate TAS - Always do this last. It needs ias, alt and oat. 
    g_tas =  calcTAS(g_ias, g_alt, g_oat);


    // If the demo loop is run, do it before the set outputs
    // thaw way the set outputs picks up the demo values
	  int btnState = digitalRead(SWITCH_PIN);
    if (btnState == LOW) {
      runDemo();  
    }

    //--------------------------------
    // Set the ouputs
    int val;
 
    // ASI
    // scale it to use it with the servo (value between 0 and 180)  - 0 to Vne
    val = map(g_ias, 0, VNE, 180, 0);  // 180 deg servo (but only goes ~ 170). Note all servos range 0-180. 270 uses gearing, thus 180 -> 270
    servoIAS.write(val);                // set the servo position according to the scaled value

    // VSI
    val = map(g_vsi, -1000, +1000, 180, 0);  // 180 deg servo (but only goes ~ 170)   
    servoVSI.write(val);                


    // Update the display
    tft_page_1();

    // End of TIMER slice
    g_ticks++;
		g_revticks++;  
    _millis = millis();
    interrupts();  // end vital section
  }
}

